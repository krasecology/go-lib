package mailing

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"gitlab.com/gbh007/gojlog"
)

const sendTimeout = time.Minute

// MailingServerConfig структура для конфигурации сервера рассылок
type MailingServerConfig struct {
	CreateURL        string `json:"create_url,omitempty"`
	CreateAndSendURL string `json:"cas_url,omitempty"`
	Token            string `json:"token,omitempty"`
}

// конфигурация сервера рассылок
var _cnf = MailingServerConfig{}

// SetConfig устанавливает конфигурацию сервера рассылок
func SetConfig(cnf MailingServerConfig) { _cnf = cnf }

// LetterAttachment вложение письма
type LetterAttachment struct {
	Name string `json:"name"` // название вложения
	Body string `json:"body"` // тело вложения base64
}

// NewAttachment создает новое вложение
func NewAttachment(name string, body interface{}) LetterAttachment {
	type XLSX interface{ Write(w io.Writer) error }
	buff := bytes.Buffer{}
	fw := base64.NewEncoder(base64.StdEncoding, &buff)
	switch f := body.(type) {
	case XLSX:
		f.Write(fw)
	case io.Reader:
		io.Copy(fw, f)
	default:
		io.WriteString(fw, fmt.Sprint(body))
	}
	fw.Close()
	return LetterAttachment{Name: name, Body: buff.String()}
}

type Letter struct {
	MailingID   string             `json:"mailing_id"`  // ид списка рассылки по которому идет отправка
	Header      string             `json:"header"`      // тема письма
	Text        string             `json:"text"`        // текст письма
	Attachments []LetterAttachment `json:"attachments"` // вложения
	To          []string           `json:"to"`          // алтернативный список получателей (только в случае если необходимо отправить на не те адреса что в рассылке)
}

// Create создает/отправляет письмо на сервер рассылок
func (l Letter) Create(send bool) bool {
	// устанавливаем урлу сервера
	var url string = _cnf.CreateURL
	if send {
		url = _cnf.CreateAndSendURL
	}
	if url == "" {
		gojlog.Warning("не указан сервер рассылок")
		return false
	}
	// подготавливаем запрос
	buff := bytes.Buffer{}
	json.NewEncoder(&buff).Encode(l)
	req, err := http.NewRequest(http.MethodPost, url, &buff)
	if err != nil {
		gojlog.Error(err)
		return false
	}
	req.Close = true
	req.Header.Set("Content-Type", "application/json")
	req.AddCookie(&http.Cookie{Name: "authsession", Value: _cnf.Token})
	// выполняем запрос
	res, err := (&http.Client{Timeout: sendTimeout}).Do(req)
	if err != nil {
		gojlog.Error(err)
		return false
	}
	defer res.Body.Close()
	if res.StatusCode < 200 || res.StatusCode > 299 {
		gojlog.Error("сообщение не отправлено ", res.Status)
		return false
	}
	return true
}
