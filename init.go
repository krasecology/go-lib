package golib

import "gitlab.com/gbh007/gojlog"

// инициализация пакета
func init() {
	// добавляем обрезку пути
	gojlog.AddTrimPathAuto()
}
