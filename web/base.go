package web

import (
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"strings"

	"gitlab.com/krasecology/go-lib/account"
	"gitlab.com/krasecology/go-lib/db/model"
	"gitlab.com/krasecology/go-lib/stat"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// CoreTokenName имя печеньки с ключом сессии
const CoreTokenName = "authsession"

// перменные ошибок
var (
	ErrNotAuth         = fmt.Errorf("необходима авторизация")
	ErrForbidden       = fmt.Errorf("нет прав доступа")
	ErrNotFound        = fmt.Errorf("данные не найдены")
	ErrParseData       = fmt.Errorf("некорректный формат данных")
	ErrDeleteData      = fmt.Errorf("невозможно удалить данные")
	ErrCreateData      = fmt.Errorf("невозможно создать данные")
	ErrAppendData      = fmt.Errorf("невозможно добавить данные")
	ErrUpdateData      = fmt.Errorf("невозможно изменить данные")
	ErrProcessingData  = fmt.Errorf("невозможно обработать данные")
	ErrTooManyRequests = fmt.Errorf("слишком много запросов")
)

// ParseJSON читает из тела запроса переданную структуру
func ParseJSON(r *http.Request, data interface{}) error {
	return json.NewDecoder(r.Body).Decode(&data)
}

// GetSessionToken получает токен из печеньки
func GetSessionToken(r *http.Request) (token primitive.ObjectID, err error) {
	cookie, err := r.Cookie(CoreTokenName)
	if err != nil {
		return
	}
	token, err = primitive.ObjectIDFromHex(cookie.Value)
	return
}

// CheckSession проверяет сессию
func CheckSession(r *http.Request) (s model.Session, u model.User, e error) {
	e = ErrNotAuth
	token, err := GetSessionToken(r)
	if err != nil {
		return
	}
	s, u, ok := account.Validate(token)
	if !ok {
		return
	}
	return s, u, nil
}

// GetIP возвращает реальный ip пользователя
func GetIP(r *http.Request) string {
	if ip := r.Header.Get("X-REAL-IP"); ip != "" {
		return ip
	}
	if ips := r.Header.Get("X-FORWARDED-FOR"); ips != "" {
		for _, ip := range strings.Split(ips, ",") {
			return ip
		}
	}
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return ""
	}
	return ip
}

// GetSUPOk возвращает данные о пользователе
// используется только после прохождения запросом PermissionHandler (вносит данные в запрос)
// возвращает сессию, пользователя
func GetSUPOk(r *http.Request) (model.Session, model.User, bool) {
	s, ok1 := r.Context().Value(ResponseSessionKey).(model.Session)
	u, ok2 := r.Context().Value(ResponseUserKey).(model.User)
	ok := ok1 && ok2
	return s, u, ok
}

// GetAccountInfo возвращает информацию об аккаунте с которого выполняется запрос
// формат выходной строки [USER_EMAIL-OR-USER_LOGIN/IP] или [unauthorized/IP]
func GetAccountInfo(r *http.Request) string {
	info := "["
	_, u, ok := GetSUPOk(r)
	if ok {
		if u.Email != "" {
			info += u.Email
		} else {
			info += u.Login
		}
	} else {
		info += "unauthorized"
	}
	info += "/" + GetIP(r) + "]"
	return info
}

// MakeStat создает информацию о статистике
func MakeStat(r *http.Request) stat.Stat {
	st := stat.Stat{
		UserAgent:     r.UserAgent(),
		Host:          r.Host,
		Url:           r.URL.String(),
		Method:        r.Method,
		RemoteIP:      r.RemoteAddr,
		RealIP:        r.Header.Get("X-REAL-IP"),
		ForwardedFor:  r.Header.Get("X-FORWARDED-FOR"),
		ContentLength: r.ContentLength,
	}
	s, u, ok := GetSUPOk(r)
	if ok {
		st.IsBot = s.IsBot
		st.UserID = u.ID.Hex()
		st.UserEmail = u.Email
		st.UserLogin = u.Login
	}
	return st
}
