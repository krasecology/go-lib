package web

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"time"

	"gitlab.com/gbh007/gojlog"
	"gitlab.com/krasecology/go-lib/db/model"
	"gitlab.com/krasecology/go-lib/stat"
)

// handleStat функция для обработки статистики
func handleStat(r *http.Request, start time.Time) {
	// если статистика отключена выходим
	if r.Context().Value(ResponseDisableStat) != nil {
		return
	}
	// собираем данные
	st := MakeStat(r)
	// устанавливаем информацию о продолжительности запроса
	st.Date = start
	st.Duration = int64(time.Now().Sub(start))
	// добавляем дополнитеьлную информацию если есть
	data := r.Context().Value(ResponseStatInfo)
	if data != nil {
		buff := bytes.Buffer{}
		if json.NewEncoder(&buff).Encode(data) == nil {
			st.Info = buff.String()
		}
	}
	// добавляем информацию об ошибке если есть
	if err := r.Context().Err(); err != nil {
		st.Error = err.Error()
	}
	// отправляем данные на сервер
	go stat.Send(st)
}

// FileWriteHandler хандлер для ответа в виде файла
func FileWriteHandler(filename string, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// обработка данных статистики
		start := time.Now()
		// обрабатываем статистику после запроса
		defer handleStat(r, start)
		// обработка запроса
		w.Header().Set("Content-Type", "application/*")
		w.Header().Set("Content-Disposition", `attachment; filename="`+filename+`"`)
		if r.Method == http.MethodOptions {
			return
		}
		if next != nil {
			next.ServeHTTP(w, r)
		}
		if err := r.Context().Err(); err != nil {
			w.Header().Set("Content-Type", "text/plain; charset=utf-8")
			switch err {
			case ErrForbidden:
				w.WriteHeader(http.StatusForbidden)
			case ErrNotFound:
				w.WriteHeader(http.StatusNotFound)
			case ErrNotAuth:
				w.WriteHeader(http.StatusUnauthorized)
			case ErrParseData:
				w.WriteHeader(http.StatusBadRequest)
			default:
				w.WriteHeader(http.StatusInternalServerError)
			}
			if _, err := io.WriteString(w, err.Error()); err != nil {
				gojlog.Error(err)
			}
			return
		}
		w.WriteHeader(http.StatusOK)
		data := r.Context().Value(ResponseDataKey)
		if data == nil {
			return
		}
		type XLSX interface {
			Write(w io.Writer) error
		}
		// определение типа данных
		// возврат делается для того чтобы не применилось несколько шаблонов
		switch f := data.(type) {
		case XLSX:
			if err := f.Write(w); err != nil {
				gojlog.Error(err)
			}
			return
		case io.Reader:
			if _, err := io.Copy(w, f); err != nil {
				gojlog.Error(err)
			}
			return
		}
	})
}

// JSONWriteHandler хандлер для ответа в виде json
func JSONWriteHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// обработка данных статистики
		start := time.Now()
		// обрабатываем статистику после запроса
		defer handleStat(r, start)
		// обработка запроса
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
		w.Header().Set("Content-Type", "application/json")
		if r.Method == http.MethodOptions {
			w.Header().Set("Allow", "POST, OPTIONS")
			return
		}
		if next != nil {
			next.ServeHTTP(w, r)
		}
		enc := json.NewEncoder(w)
		if err := r.Context().Err(); err != nil {
			switch err {
			case ErrForbidden:
				w.WriteHeader(http.StatusForbidden)
			case ErrNotFound:
				w.WriteHeader(http.StatusNotFound)
			case ErrNotAuth:
				w.WriteHeader(http.StatusUnauthorized)
			case ErrParseData:
				w.WriteHeader(http.StatusBadRequest)
			case ErrTooManyRequests:
				w.WriteHeader(http.StatusTooManyRequests)
			default:
				w.WriteHeader(http.StatusInternalServerError)
			}
			if err := enc.Encode(err.Error()); err != nil {
				gojlog.Error(err)
			}
			return
		}
		if tmp, ok := r.Context().Value(ResponseRedirectKey).(string); ok {
			http.Redirect(w, r, tmp, http.StatusTemporaryRedirect)
			return
		}
		w.WriteHeader(http.StatusOK)
		data := r.Context().Value(ResponseDataKey)
		if data == nil {
			return
		}
		if err := enc.Encode(data); err != nil {
			gojlog.Error(err)
		}
	})
}

// PermissionHandler проверяет наличие необходимых привелегий
func PermissionHandler(permName string, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, u, login := GetSUPOk(r)
		if !login {
			SetError(r, ErrNotAuth)
			return
		}
		ok := false
		for _, p := range u.CalculatedPermissions {
			if p == permName {
				ok = true
				break
			}
		}
		if !ok {
			SetError(r, ErrForbidden)
		} else {
			next.ServeHTTP(w, r)
		}
	})
}

// MultiPermissionHandler проверяет наличие необходимых привелегий
func MultiPermissionHandler(permNames []string, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, u, login := GetSUPOk(r)
		if !login {
			SetError(r, ErrNotAuth)
			return
		}
		for _, permName := range permNames {
			ok := false
			for _, p := range u.CalculatedPermissions {
				if p == permName {
					ok = true
					break
				}
			}
			if !ok {
				SetError(r, ErrForbidden)
				return
			}
		}
		next.ServeHTTP(w, r)
	})
}

// SessionHandler обработчик сессии пользователя
func SessionHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s, u, err := CheckSession(r)
		if err == nil {
			// устанавливаем в контекст данные пользователя
			model.UpdateSessionWithIP(s.ID, GetIP(r))
			SetData(r, ResponseUserKey, u)
			SetData(r, ResponseSessionKey, s)
		}
		next.ServeHTTP(w, r)
	})
}

// AuthRequireHandler проверяет авторизован ли пользователь (отсекает 401 ошибкой не авторизованных)
func AuthRequireHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _, login := GetSUPOk(r)
		if !login {
			SetError(r, ErrNotAuth)
			return
		}
		next.ServeHTTP(w, r)
	})
}

// UserTypeRequireHandler проверяет тип пользователя
// только один параметр может быть истиной одновременно!
func UserTypeRequireHandler(user, bot bool, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s, _, login := GetSUPOk(r)
		if !login {
			SetError(r, ErrNotAuth)
			return
		}
		if user && s.IsBot {
			SetError(r, ErrForbidden)
			return
		}
		if bot && !s.IsBot {
			SetError(r, ErrForbidden)
			return
		}
		next.ServeHTTP(w, r)
	})
}

// AddHandler добавляет в мукс обработчик JSON и если привелегия не пустая, то обработчик привелегий
func AddHandler(mux *http.ServeMux, uri, perm string, next http.Handler) {
	if perm != "" {
		mux.Handle(uri, JSONWriteHandler(SessionHandler(PermissionHandler(perm, next))))
	} else {
		mux.Handle(uri, JSONWriteHandler(SessionHandler(next)))
	}
}

// AuthHandlerConfig конфигурация для проверки пользователя и прав (усиленная версия AddHandler:OVER9000),
// если установлен параметр Filename то будет обрабатывать весь запрос как файл,
// установка Perm или Perms игнорирует проверку LoggedRequire (устанавливает в true),
// установка Perm вызывает игнор Perms (устанавливает в []),
// установка OnlyUser вызывает игнор OnlyBot (устанавливается в false)
type AuthHandlerConfig struct {
	Mux           *http.ServeMux // мукс для добавления
	URI           string         // урла в МУКСЕ по которой будет доступен обработчик
	Perm          string         // требуемая привелегия
	Perms         []string       // требуемая привелегии
	OnlyUser      bool           // только для пользователей
	OnlyBot       bool           // только для ботов
	LoggedRequire bool           // обязательна авторизация
	Next          http.Handler   // обработчик
	Filename      string         // имя файла для загрузки
}

// Apply применяет конфигурацию к муксу
func (ahc AuthHandlerConfig) Apply() {
	// флаги для скипа некоторых этапов
	permsRequire := false
	userTypeRequire := false

	// начинаем сборку цепи обработчиков с конца

	// устанавливаем конечный обработчик
	handler := ahc.Next

	// устанавливаем обработчик привелегий
	if ahc.Perm != "" {
		// если только 1 привелегия
		handler = PermissionHandler(ahc.Perm, handler)
		permsRequire = true
	} else if len(ahc.Perms) > 0 {
		// если требуется несколько привелегий
		handler = MultiPermissionHandler(ahc.Perms, handler)
		permsRequire = true
	}

	// устанавливаем обработчик типа пользователя
	if ahc.OnlyUser {
		// только для пользователей
		handler = UserTypeRequireHandler(true, false, handler)
		userTypeRequire = true
	} else if ahc.OnlyBot {
		// только для ботов
		handler = UserTypeRequireHandler(false, true, handler)
		userTypeRequire = true
	}

	// устанавливаем обработчик входа в аккаунт
	if !permsRequire && !userTypeRequire && ahc.LoggedRequire {
		// только если не было предыдущих этапов (они уже отсекают неавторизированных)
		handler = AuthRequireHandler(handler)
	}

	// устанавливаем обработчик сессии
	// он подгрузит данные в контекст, они НУЖНЫ для остальных обработчиков
	handler = SessionHandler(handler)

	// устанавливаем конечный обработчик формы вывода
	if ahc.Filename != "" {
		// для ответа в виде файла
		handler = FileWriteHandler(ahc.Filename, handler)
	} else {
		// для ответа в JSON
		handler = JSONWriteHandler(handler)
	}

	// добавляем цепочку обработчиков в мукс
	ahc.Mux.Handle(ahc.URI, handler)
}
