package web

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"gitlab.com/krasecology/go-lib/account"

	"gitlab.com/gbh007/gojlog"
)

// Logout завершает пользовательскую сессию
func Logout() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, err := GetSessionToken(r)
		if err != nil {
			gojlog.Error(err)
			SetError(r, ErrProcessingData)
			return
		}
		id, err := account.Logout(token)
		if err != nil {
			SetError(r, ErrProcessingData)
			return
		}
		SetResponse(r, struct{}{})
		// удаление токена сессии
		if strings.Index(r.Header.Get("Origin"), "krasecology.ru") != -1 {
			http.SetCookie(w, &http.Cookie{
				Name:     CoreTokenName,
				Value:    "",
				Path:     "/",
				HttpOnly: true,
				Expires:  time.Unix(0, 0),
				Domain:   "krasecology.ru",
			})
		}
		http.SetCookie(w, &http.Cookie{
			Name:     CoreTokenName,
			Value:    "",
			Path:     "/",
			HttpOnly: true,
			Expires:  time.Unix(0, 0),
		})
		gojlog.Infof("завершена сессия %s ip %s", id.Hex(), GetIP(r))
	})
}

// Login создает новую пользовательскую сессию
func Login() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := struct {
			Login string `json:"login"`
			Pass  string `json:"pass"`
		}{}
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		token, ok := account.Login(req.Login, req.Pass)
		if !ok {
			SetError(r, fmt.Errorf("неверный логин/пароль"))
			return
		}
		SetResponse(r, struct{}{})
		if strings.Index(r.Header.Get("Origin"), "krasecology.ru") != -1 {
			http.SetCookie(w, &http.Cookie{
				Name:     CoreTokenName,
				Value:    token.Hex(),
				Path:     "/",
				HttpOnly: true,
				Domain:   "krasecology.ru",
			})
			// удаляем корневой токен на поддомене если есть
			http.SetCookie(w, &http.Cookie{
				Name:     CoreTokenName,
				Value:    "",
				Path:     "/",
				HttpOnly: true,
				Expires:  time.Unix(0, 0),
			})
		} else {
			http.SetCookie(w, &http.Cookie{
				Name:     CoreTokenName,
				Value:    token.Hex(),
				Path:     "/",
				HttpOnly: true,
			})
		}
		gojlog.Infof("создана сессия %s для %s ip %s", token.Hex(), req.Login, GetIP(r))
	})
}
