package web

import (
	"net/http"

	"gitlab.com/krasecology/go-lib/account"
	"gitlab.com/krasecology/go-lib/db/model"
	"gitlab.com/krasecology/go-lib/global"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Info возвращает информацию об аккаунте
func Info() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, err := GetSessionToken(r)
		if err != nil {
			SetError(r, ErrNotAuth)
			return
		}
		s, u, ok := account.Validate(token)
		if !ok {
			SetError(r, ErrNotAuth)
			return
		}
		type hasAndVerified struct {
			Data     string `json:"data"`
			Exist    bool   `json:"exist"`
			Verified bool   `json:"verified"`
		}
		resp := struct {
			ID           primitive.ObjectID `json:"id"`
			Login        string             `json:"login"`
			Email        hasAndVerified     `json:"email"`
			Phone        hasAndVerified     `json:"phone"`
			Name         model.Name         `json:"name"`
			Username     string             `json:"username"`
			LastCheck    string             `json:"last_check"`
			LastAction   string             `json:"last_action"`
			Perms        []string           `json:"permissions"`
			Organisation string             `json:"organisation"`
		}{
			ID:           u.ID,
			Login:        u.Login,
			Email:        hasAndVerified{Data: u.Email},
			Phone:        hasAndVerified{Data: u.Phone},
			Name:         u.Name,
			Username:     u.GetUsername(),
			LastCheck:    global.PrettyTime(s.LastCheck),
			LastAction:   global.PrettyTime(u.LastAction),
			Perms:        u.CalculatedPermissions,
			Organisation: u.Organisation,
		}
		if u.Email != "" {
			resp.Email.Exist = true
			resp.Email.Verified = u.Flags.VerifiedEmail
		}
		if u.Phone != "" {
			resp.Phone.Exist = true
			resp.Phone.Verified = u.Flags.VerifiedPhone
		}
		model.UpdateSession(s.ID)
		SetResponse(r, resp)
	})
}
