package global

import (
	"time"

	"gitlab.com/gbh007/gojlog"
)

// PrettyTime форматирует время в читабельный для пользователя вид в местной локации
func PrettyTime(t time.Time) string {
	if t.IsZero() {
		return ""
	}
	l, err := time.LoadLocation(BaseLocation)
	if err != nil {
		gojlog.Error(err)
		return "error"
	}
	return t.In(l).Format(PrettyTimeFormat)
}
