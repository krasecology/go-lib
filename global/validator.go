package global

import "regexp"

var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

// IsEmailValid проверяет почту на валидность
func IsEmailValid(email string) bool {
	if len(email) < 3 && len(email) > 254 {
		return false
	}
	return emailRegex.MatchString(email)
}

var phoneRegex = regexp.MustCompile(`^\+7[0-9]{10}$`)

// IsPhoneValid проверяет телефон на валидность
func IsPhoneValid(phone string) bool {
	if len(phone) != 12 {
		return false
	}
	return phoneRegex.MatchString(phone)
}

var loginRegex = regexp.MustCompile(`^[a-zA-Z0-9\._-]+$`)

// IsLoginValid проверяет логин на валидность
func IsLoginValid(login string) bool {
	if len(login) < 4 || len(login) > 20 {
		return false
	}
	return loginRegex.MatchString(login)
}

// IsPasswordValid проверяет пароль на валидность
func IsPasswordValid(password string) bool {
	if len(password) > 4 && len(password) < 100 {
		return true
	}
	return false
}
