package global

// BaseLocation базовая часовой пояс приложения
const BaseLocation = "Asia/Krasnoyarsk"

// PrettyTimeFormat стандартный формат времени для вывода строкой (при ответе сервера)
const PrettyTimeFormat = "02.01.2006 15:04:05"
