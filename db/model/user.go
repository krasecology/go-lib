package model

import (
	"time"

	"gitlab.com/krasecology/go-lib/db/connect"

	"gitlab.com/gbh007/gojlog"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// MailRegistrationToken токен для подтверждения электроной почты
type MailRegistrationToken struct {
	ExpirationTime time.Time `json:"expiration_time" bson:"expiration_time"`
	Token          string    `json:"token" bson:"token"`
	Email          string    `json:"email" bson:"email"`
}

// PasswordResetToken токен для сброса пароля
type PasswordResetToken struct {
	ExpirationTime time.Time `json:"expiration_time" bson:"expiration_time"`
	Token          string    `json:"token" bson:"token"`
}

// Name ФИО пользователя
type Name struct {
	Firstname  string `json:"firstname" bson:"firstname"`
	Lastname   string `json:"lastname" bson:"lastname"`
	Patronymic string `json:"patronymic" bson:"patronymic"`
}

// GetUsername возвращает имя пользователя,
// предпочтение отдается фамилии, потом первая буква имени с точкой, потом первая буква отчества с точкой
// в случае если не указана фамилия, то используется alter
func (name Name) GetUsername(alter string) string {
	if name.Lastname == "" {
		return alter
	}
	username := name.Lastname
	if name.Firstname != "" {
		// string([]rune(СТРОКА)[0]) берет первый символ в строке, с учетом юникода
		username += " " + string([]rune(name.Firstname)[0]) + "."
	}
	// типо фича)
	// в случае если не указано имя, но указано отчество оно будет использоватся
	if name.Patronymic != "" {
		// string([]rune(СТРОКА)[0]) берет первый символ в строке, с учетом юникода
		username += " " + string([]rune(name.Patronymic)[0]) + "."
	}
	return username
}

// GetFullname возвращает ФИО пользователя
func (name Name) GetFullname() string {
	fullname := name.Lastname
	if name.Firstname != "" {
		fullname += " " + name.Firstname
	}
	if name.Patronymic != "" {
		fullname += " " + name.Patronymic
	}
	return fullname
}

// User структура для данных пользователя
// логин и почта в базе хранятся в нижнем регистре
type User struct {
	ID                      primitive.ObjectID    `json:"id" bson:"_id"`
	Login                   string                `json:"login" bson:"login"`
	Email                   string                `json:"email" bson:"email"`
	Phone                   string                `json:"phone" bson:"phone"`
	Password                string                `json:"password" bson:"password"`
	Salt                    string                `json:"salt" bson:"salt"`
	Organisation            string                `json:"organisation" bson:"organisation"`
	Name                    Name                  `json:"name" bson:"name"`
	Flags                   Flags                 `json:"flags" bson:"flags"`
	Permissions             []primitive.ObjectID  `json:"permissions" bson:"permissions"`
	Groups                  []primitive.ObjectID  `json:"groups" bson:"groups"`
	LastAction              time.Time             `json:"last_action" bson:"last_action"`
	Registration            time.Time             `json:"registration" bson:"registration"`
	CalculatedPermissions   []string              `json:"cp" bson:"cp"`
	CalculatedPermissionIDs []primitive.ObjectID  `json:"cpids" bson:"cpi"`
	MRT                     MailRegistrationToken `json:"mrt" bson:"mrt"`
	PRT                     PasswordResetToken    `json:"prt" bson:"prt"`
	KEID                    int                   `json:"ke_id" bson:"ke_id"`
}

// Flags флаги пользователя
type Flags struct {
	Banned        bool `json:"banned" bson:"banned"`
	Bot           bool `json:"bot" bson:"bot"`
	VerifiedEmail bool `json:"verified_email" bson:"verified_email"`
	VerifiedPhone bool `json:"verified_phone" bson:"verified_phone"`
	NotConfirmed bool `json:"not_confirmed" bson:"not_confirmed"`
}

// GetUsername возвращает имя пользователя,
// предпочтение отдается фамилии, потом первая буква имени с точкой, потом первая буква отчества с точкой
// в случае если не указана фамилия, то используется логин
func (user User) GetUsername() string {
	return user.Name.GetUsername(user.Login)
}

// GetByLogin возвращает пользователя по логину
func GetByLogin(login string) (User, error) {
	ctx, cnl := connect.NewTimeoutContext(nil)
	defer cnl()
	user := User{}
	res := _mongoDB.Collection(userCollectionName).FindOne(ctx, bson.M{"login": login})
	err := res.Err()
	if err != nil {
		if err != mongo.ErrNoDocuments {
			gojlog.Error(err)
		}
		return user, err
	}
	err = res.Decode(&user)
	if err != nil {
		gojlog.Error(err)
	}
	return user, err
}

// GetByEmail возвращает пользователя по email, только для подтвержденных почт
func GetByEmail(email string) (User, error) {
	ctx, cnl := connect.NewTimeoutContext(nil)
	defer cnl()
	user := User{}
	res := _mongoDB.Collection(userCollectionName).FindOne(ctx, bson.M{"email": email, "flags.verified_email": true})
	err := res.Err()
	if err != nil {
		if err != mongo.ErrNoDocuments {
			gojlog.Error(err)
		}
		return user, err
	}
	err = res.Decode(&user)
	if err != nil {
		gojlog.Error(err)
	}
	return user, err
}

// GetByPhone возвращает пользователя по телефону, только для подтвержденных телефонов
func GetByPhone(phone string) (User, error) {
	ctx, cnl := connect.NewTimeoutContext(nil)
	defer cnl()
	user := User{}
	res := _mongoDB.Collection(userCollectionName).FindOne(ctx, bson.M{"phone": phone, "flags.verified_phone": true})
	err := res.Err()
	if err != nil {
		if err != mongo.ErrNoDocuments {
			gojlog.Error(err)
		}
		return user, err
	}
	err = res.Decode(&user)
	if err != nil {
		gojlog.Error(err)
	}
	return user, err
}

// GetByID возвращает пользователя по ID
func GetByID(id primitive.ObjectID) (User, error) {
	user := User{}
	ctx, cnl := connect.NewTimeoutContext(nil)
	defer cnl()
	res := _mongoDB.Collection(userCollectionName).FindOne(ctx, bson.M{"_id": id})
	err := res.Err()
	if err != nil {
		if err != mongo.ErrNoDocuments {
			gojlog.Error(err)
		}
		return user, err
	}
	err = res.Decode(&user)
	if err != nil {
		gojlog.Error(err)
	}
	return user, err
}

// UpdateActivity обновляет активность пользователя
func UpdateActivity(userID primitive.ObjectID) {
	ctx, cnl := connect.NewTimeoutContext(nil)
	defer cnl()
	_, err := _mongoDB.Collection(userCollectionName).UpdateOne(
		ctx,
		bson.M{"_id": userID},
		bson.M{
			"$set": bson.M{
				"last_action": time.Now(),
			},
		},
	)
	if err != nil {
		gojlog.Error(err)
	}
}
