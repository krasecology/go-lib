package model

import (
	"gitlab.com/gbh007/gojlog"
	"gitlab.com/krasecology/go-lib/db/connect"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// названия коллекций в монго
const (
	userCollectionName    = "users"
	sessionCollectionName = "sessions"
)

var _mongoDB *mongo.Database

// Connect подключается к основной бд сервера авторизации
func Connect(uri, dbName, user, password string) error {
	ctx, cnl := connect.NewTimeoutContext(nil)
	defer cnl()
	opts := options.Client().ApplyURI(
		uri,
	)
	if user != "" {
		opts.SetAuth(
			options.Credential{
				Username: user,
				Password: password,
			},
		)
	}
	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		gojlog.Error(err)
		return err
	}
	err = client.Ping(ctx, nil)
	if err != nil {
		gojlog.Error(err)
		return err
	}
	_mongoDB = client.Database(dbName)
	return nil
}
