package model

import (
	"time"

	"gitlab.com/krasecology/go-lib/db/connect"

	"gitlab.com/gbh007/gojlog"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// Session структура для пользовательской сессии
type Session struct {
	ID        primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	UserID    primitive.ObjectID `json:"user_id" bson:"user_id"`
	Created   time.Time          `json:"created" bson:"created"`
	LastCheck time.Time          `json:"last_check" bson:"last_check,omitempty"`
	IsBot     bool               `json:"is_bot" bson:"is_bot"`
	LastIP    string             `json:"last_ip" bson:"last_ip,omitempty"`
}

// CreateSession создает новую сессию
func CreateSession(userID primitive.ObjectID, isBot bool) (id primitive.ObjectID, err error) {
	ctx, cnl := connect.NewTimeoutContext(nil)
	defer cnl()
	res, err := _mongoDB.Collection(sessionCollectionName).InsertOne(ctx, Session{
		UserID:  userID,
		Created: time.Now(),
		IsBot:   isBot,
	})
	if err != nil {
		gojlog.Error(err)
		return
	}
	id = res.InsertedID.(primitive.ObjectID)
	return
}

// DeleteSession удаляет сессию
func DeleteSession(id primitive.ObjectID) error {
	ctx, cnl := connect.NewTimeoutContext(nil)
	defer cnl()
	_, err := _mongoDB.Collection(sessionCollectionName).DeleteOne(ctx, bson.M{"_id": id})
	if err != nil {
		gojlog.Error(err)
	}
	return err
}

// GetSession возвращает сессию
func GetSession(id primitive.ObjectID) (Session, error) {
	session := Session{}
	ctx, cnl := connect.NewTimeoutContext(nil)
	defer cnl()
	res := _mongoDB.Collection(sessionCollectionName).FindOne(ctx, bson.M{"_id": id})
	err := res.Err()
	if err != nil {
		if err != mongo.ErrNoDocuments {
			gojlog.Error(err)
		}
		return session, err
	}
	err = res.Decode(&session)
	if err != nil {
		gojlog.Error(err)
	}
	return session, err
}

// UpdateSession обновляет информацию о последней проверки прав в сессии
func UpdateSession(id primitive.ObjectID) {
	ctx, cnl := connect.NewTimeoutContext(nil)
	defer cnl()
	_, err := _mongoDB.Collection(sessionCollectionName).UpdateOne(
		ctx,
		bson.M{"_id": id},
		bson.M{
			"$set": bson.M{"last_check": time.Now()},
		},
	)
	if err != nil {
		gojlog.Error(err)
	}
}

// UpdateSessionWithIP обновляет информацию о последней проверки прав в сессии с последним ip
func UpdateSessionWithIP(id primitive.ObjectID, ip string) {
	ctx, cnl := connect.NewTimeoutContext(nil)
	defer cnl()
	_, err := _mongoDB.Collection(sessionCollectionName).UpdateOne(
		ctx,
		bson.M{"_id": id},
		bson.M{
			"$set": bson.M{
				"last_check": time.Now(),
				"last_ip":    ip,
			},
		},
	)
	if err != nil {
		gojlog.Error(err)
	}
}
