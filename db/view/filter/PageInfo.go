package filter

// PageInfo информация о странице
// используется в случае возврата значений постранично
// Current - текущая страница
// Len - длина страницы (в элементах на страницу)
// Count - количество страниц
// Items - количество элементов на данной странице
// ItemsTotal - количество элементов всего
type PageInfo struct {
	Current    int64 `json:"current"`
	Len        int64 `json:"len"`
	Count      int64 `json:"count"`
	Items      int64 `json:"items"`
	ItemsTotal int64 `json:"items_total"`
}

// Calc расчитывает параметры страницы по данным фильтра
func (pi *PageInfo) Calc(count, offset, all, items int64) *PageInfo {
	// случай когда все данные выгружаются на 1 страницу (выгрузка без пагинации или ограничений)
	if count < 1 {
		pi.Current = 1
		pi.Len = all
		pi.Count = 1
		pi.Items = items
		pi.ItemsTotal = all
		return pi
	}
	// смещаем на первый элемент после сдвига
	offset++
	pi.Current = offset / count
	if offset%count > 0 {
		pi.Current++
	}
	pi.Len = count
	pi.Count = all / count
	if all%count > 0 || all == 0 {
		pi.Count++
	}
	pi.Items = items
	pi.ItemsTotal = all
	return pi
}
