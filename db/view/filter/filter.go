package filter

import (
	"bytes"
	"encoding/json"
	"regexp"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// PageFilter базовый фильтр с управлением количества и порядка элементов
type PageFilter struct {
	MaxCount int64 `json:"count,omitempty"`
	Offset   int64 `json:"offset,omitempty"`
	Reverse  bool  `json:"reverse,omitempty"`
}

// ToOption переводит параметры фильтра в опции для запроса монго
func (pf PageFilter) ToOption(sortFieldName string) *options.FindOptions {
	opt := options.Find()
	// switch заместо нескольких if
	// если строка пустая то просто игнорирует, иначе выставляет сортировку по полю
	switch {
	case sortFieldName == "":
	case pf.Reverse:
		opt.SetSort(bson.M{sortFieldName: -1})
	case !pf.Reverse:
		opt.SetSort(bson.M{sortFieldName: 1})
	}
	if pf.MaxCount > 0 {
		opt.SetLimit(int64(pf.MaxCount))
	}
	if pf.Offset > 0 {
		opt.SetSkip(int64(pf.Offset))
	}
	return opt
}

// TimeFilter фильтр для поля времени
// имеет возможность фильтровать по диапазону
type TimeFilter struct {
	Min time.Time `json:"min,omitempty"`
	Max time.Time `json:"max,omitempty"`
}

// ToBson конвертирует фильтр в данные для bson mongo
// перед присвоением полю нужна проверка на nil иначе возможно получить некоректный результат
func (f TimeFilter) ToBson() bson.M {
	exist := false
	filter := bson.M{}
	// если значение не пустое то выставляем фильтр
	if !f.Min.IsZero() {
		filter["$gte"] = f.Min
		exist = true
	}
	// если значение не пустое то выставляем фильтр
	if !f.Max.IsZero() {
		filter["$lte"] = f.Max
		exist = true
	}
	if exist {
		return filter
	}
	return nil
}

// Apply применяет фильтр
// filter объект фильтра
// name название поля в bson
func (f TimeFilter) Apply(filter bson.M, name string) bool {
	if tf := f.ToBson(); tf != nil {
		filter[name] = tf
		return true
	}
	return false
}

// TextFilter фильтр для поиска текста (с возможностью использовать регулярные выражения)
type TextFilter string

// Apply применяет фильтр
// filter объект фильтра
// name название поля в bson
// regexp поиск по регулярному выражению (без учета регистра)
func (f TextFilter) Apply(filter bson.M, name string, regexp bool) bool {
	if f == "" {
		return false
	}
	if regexp {
		filter[name] = bson.M{
			"$regex":   f,
			"$options": "i",
		}
	} else {
		filter[name] = f
	}
	return true
}

// MatchTextFilter фильтр для поиска совпадений текста (экранирует регулярные выражения)
type MatchTextFilter string

// Apply применяет фильтр
// filter объект фильтра
// name название поля в bson
func (f MatchTextFilter) Apply(filter bson.M, name string) bool {
	if f == "" {
		return false
	}
	filter[name] = bson.M{
		"$regex":   regexp.QuoteMeta(string(f)),
		"$options": "i",
	}
	return true
}

// MultiChoiseFilter фильтр для множественого выбора простого поля
type MultiChoiseFilter []interface{}

// Apply применяет фильтр
// filter объект фильтра
// name название поля в bson
func (f MultiChoiseFilter) Apply(filter bson.M, name string) bool {
	if len(f) == 0 {
		return false
	}
	filter[name] = bson.M{"$in": f}
	return true
}

// MultiChoiseIDFilter фильтр для множественого выбора ObjectId
type MultiChoiseIDFilter []primitive.ObjectID

// Apply применяет фильтр
// filter объект фильтра
// name название поля в bson
func (f MultiChoiseIDFilter) Apply(filter bson.M, name string) bool {
	if len(f) == 0 {
		return false
	}
	filter[name] = bson.M{"$in": f}
	return true
}

// BoolFilter фильтр для полей с выбором да/нет
type BoolFilter int

// Apply применяет фильтр
// filter объект фильтра
// name название поля в bson
func (f BoolFilter) Apply(filter bson.M, name string) bool {
	switch f {
	case 1:
		filter[name] = true
	case 2:
		filter[name] = false
	default:
		return false
	}
	return true
}

// MatchNumberFilter фильтр для поиска совпадений числа (экранирует регулярные выражения)
type MatchNumberFilter struct {
	*int64
}

// Apply применяет фильтр
// filter объект фильтра
// name название поля в bson
func (f *MatchNumberFilter) Apply(filter bson.M, name string) bool {
	if f.int64 == nil {
		return false
	}
	// находим точное совпадение
	filter[name] = bson.M{
		"$eq": *f.int64,
	}
	return true
}

func (f *MatchNumberFilter) UnmarshalJSON(data []byte) error {
	if bytes.Equal([]byte("null"), data) {
		return nil
	}
	var i int64
	err := json.Unmarshal(data, &i)
	if err != nil {
		return err
	}
	f.int64 = &i
	return nil
}
