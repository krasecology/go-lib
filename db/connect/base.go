package connect

import (
	"context"
	"time"
)

// NewTimeoutContext создает контекст с таймаутом для базы
func NewTimeoutContext(parent context.Context) (context.Context, context.CancelFunc) {
	if parent == nil {
		parent = context.Background()
	}
	return context.WithTimeout(parent, time.Minute)
}
