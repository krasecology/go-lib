package stat

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/gbh007/gojlog"
)

// _statURL урла для отправки статистики
var _statURL string

// SetStatURL устанавливает урлу для статистики
func SetStatURL(URL string) {
	_statURL = URL
}

// Stat статистика
type Stat struct {
	Date          time.Time `json:"date"`
	Duration      int64     `json:"duration"`
	UserAgent     string    `json:"user_agent"`
	Host          string    `json:"host"`
	Url           string    `json:"url"`
	Method        string    `json:"method"`
	ContentLength int64     `json:"content_length"`
	RemoteIP      string    `json:"remote_ip"`
	RealIP        string    `json:"real_ip"`
	ForwardedFor  string    `json:"forwarded_for"`
	UserID        string    `json:"user_id"`
	UserEmail     string    `json:"user_email"`
	UserLogin     string    `json:"user_login"`
	IsBot         bool      `json:"is_bot"`
	Info          string    `json:"info"`
	Error         string    `json:"error"`
}

// Send отправляет статистику на сервер
func Send(s Stat) bool {
	if _statURL == "" {
		return false
	}
	// подготавливаем запрос
	buff := bytes.Buffer{}
	json.NewEncoder(&buff).Encode(s)
	req, err := http.NewRequest(http.MethodPost, _statURL, &buff)
	if err != nil {
		gojlog.Error(err)
		return false
	}
	req.Close = true
	req.Header.Set("Content-Type", "application/json")
	// выполняем запрос
	res, err := (&http.Client{Timeout: time.Minute}).Do(req)
	if err != nil {
		gojlog.Error(err)
		return false
	}
	defer res.Body.Close()
	if res.StatusCode < 200 || res.StatusCode > 299 {
		gojlog.Error("статистика не отправлена ", res.Status)
		return false
	}
	return true
}
