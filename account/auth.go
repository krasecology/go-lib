package account

import (
	"crypto/md5"
	"fmt"
	"strings"
	"time"

	"gitlab.com/krasecology/go-lib/db/model"
	"gitlab.com/krasecology/go-lib/global"

	"gitlab.com/gbh007/gojlog"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// SaltThePass возвращает посоленый пароль
func SaltThePass(pass, salt string) string { return hash(pass + salt) }

// RandomSalt возвращает рандомную соль, в случае высокой нагрузки требуется переписать с добавление рандома и т.д.
func RandomSalt(k string) string { return hash(time.Now().String() + k) }

func hash(s string) string { return fmt.Sprintf("%x", md5.Sum([]byte(s))) }

// Login создает новую пользовательскую сессию
// login - в зависимости от проверки валидности может быть
// использован как логин, телефон, электронная почта
func Login(login, pass string) (token primitive.ObjectID, ok bool) {
	login = strings.ToLower(login)
	var (
		u         model.User
		err       error
		loginType string
	)
	switch {
	case global.IsEmailValid(login):
		u, err = model.GetByEmail(login)
		loginType = "почта"
	case global.IsPhoneValid(login):
		u, err = model.GetByPhone(login)
		loginType = "телефон"
	case global.IsLoginValid(login):
		u, err = model.GetByLogin(login)
		loginType = "логин"
	default:
		gojlog.Infof("%s неизвестный тип входа", login)
		return
	}
	if err != nil {
		gojlog.Infof("%s %s отсутствует в базе", loginType, login)
		return
	}
	model.UpdateActivity(u.ID)
	// отсекаем не завершенную регистрацию
	if u.Flags.NotConfirmed {
		gojlog.Infof("%s не завершена регистрация", login)
		return
	}
	// отсекаем ботов и забанненых
	if u.Flags.Banned || u.Flags.Bot {
		gojlog.Infof("%s запрещена авторизация, или он бот", login)
		return
	}
	// выполняем проверку пароля, поскольку она более затратна чем проверки выше,
	// то используем ее только после них
	if !CheckPassword(u, pass) {
		gojlog.Infof("%s неверный пароль", login)
		return
	}
	token, err = model.CreateSession(u.ID, false)
	if err != nil {
		return
	}
	return token, true
}

// Logout завершает пользовательскую сессию
func Logout(token primitive.ObjectID) (id primitive.ObjectID, err error) {
	s, err := model.GetSession(token)
	if err != nil {
		return
	}
	u, err := model.GetByID(s.UserID)
	if err != nil {
		return
	}
	model.UpdateActivity(u.ID)
	return s.ID, model.DeleteSession(token)
}

// CheckPassword проверяет пароль пользователя
func CheckPassword(u model.User, pass string) bool {
	if SaltThePass(pass, u.Salt) != u.Password {
		return false
	}
	return true
}

// SaltedPassword солит пароль простой солью
func SaltedPassword(password string) (pass string, salt string) {
	salt = RandomSalt("")
	pass = SaltThePass(password, salt)
	return
}

// NewBotToken создает новый токен для бота
func NewBotToken(userID primitive.ObjectID) (token primitive.ObjectID, err error) {
	u, err := model.GetByID(userID)
	if err != nil {
		return
	}
	model.UpdateActivity(u.ID)
	// отсекаем не завершенную регистрацию
	if u.Flags.NotConfirmed {
		gojlog.Infof("%s не завершена регистрация", u.Login)
		return token, fmt.Errorf("не завершена регистрация")
	}
	// отсекаем НЕ ботов и забанненых
	if u.Flags.Banned || !u.Flags.Bot {
		gojlog.Infof("%s заблокирован, или он не бот", u.Login)
		return token, fmt.Errorf("пользователь не бот")
	}
	token, err = model.CreateSession(u.ID, true)
	if err != nil {
		return
	}
	return
}

func getSessionAndUser(token primitive.ObjectID) (s model.Session, u model.User, err error) {
	s, err = model.GetSession(token)
	if err != nil {
		return
	}
	u, err = model.GetByID(s.UserID)
	return
}

// Validate выполняет полную проверку сессии
func Validate(token primitive.ObjectID) (s model.Session, u model.User, ok bool) {
	s, u, err := getSessionAndUser(token)
	if err != nil {
		return
	}
	// если пользователь не подтвержден, то валидация провалена
	if u.Flags.NotConfirmed {
		return
	}
	// если пользователь заблокирован, то валидация провалена
	if u.Flags.Banned {
		return
	}
	ok = true
	// если это сессия для бота, а аккаунт не бот, то валидация провалена
	// и если это сессия для человека, а аккаунт бот, то валидация провалена
	if s.IsBot != u.Flags.Bot {
		ok = false
	}

	return
}
