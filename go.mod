module gitlab.com/krasecology/go-lib

go 1.15

require (
	gitlab.com/gbh007/gojlog v1.3.1
	go.mongodb.org/mongo-driver v1.5.3
)
